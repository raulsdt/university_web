package com.autentia.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.autentia.bean.ProfesorBean;


@Repository
public interface ProfesorRepository extends ProfesorDAO{
	
	@Select("SELECT * FROM PROFESOR")
	@Results(value={
		@Result(/*id=true,*/property="id",column="idProfesor"),
		@Result(property="nombreProfesor", column="nombreProfesor")
	})
	@Override
    public List<ProfesorBean> selectAll();
       
	@Select("SELECT idProfesor, nombreProfesor FROM PROFESOR WHERE idProfesor = #{idPr}")
	@Results({
			@Result(/*id=true,*/property="id",column="idProfesor"),
			@Result(property="nombreProfesor", column="nombreProfesor")
		})
	@Override
    public ProfesorBean selectProfesorById(@Param("idPr") int idPr);
}
