package com.autentia.bean;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.autentia.common.BusinessEntity;


/**
 * Clase bean de Profesor
 * @author Ra�l S.
 *
 */

public class ProfesorBean extends BusinessEntity implements Serializable{
	
	
	private static final long serialVersionUID = 8554042246904977061L;

	String nombreProfesor;
	
	
	public ProfesorBean(){
	}
	
	public ProfesorBean(int id, String nombreProfesor) {
		super();
		setId(id);
		this.nombreProfesor = nombreProfesor;
	}
	

	public String getNombreProfesor() {
		return nombreProfesor;
	}

	public void setNombreProfesor(String nombreProfesor) {
		this.nombreProfesor = nombreProfesor;
	}


	
	@Override
	public int hashCode(){
	    return new HashCodeBuilder()
	        .append(getId())
	        .toHashCode();
	}

	@Override
	public boolean equals(final Object obj){
		if (obj == null) { return false; }
		   if (obj == this) { return true; }
		   if (obj.getClass() != getClass()) {
		     return false;
		   }
		   ProfesorBean other  = (ProfesorBean) obj;
	        return new EqualsBuilder()
	        	.appendSuper(super.equals(obj))
	            .append(nombreProfesor, other.nombreProfesor)
	            .isEquals();
	    
	}
}
