package com.autentia.services;

import java.util.List;

import com.autentia.bean.CursoBean;

public interface CursoService {
	
	public List<CursoBean> selectAll();
	
	public void create(CursoBean c);
	

	
}
