package com.autentia.services;

import java.util.List;

import com.autentia.bean.ProfesorBean;

public interface ProfesorService {
	public List<ProfesorBean> selectAll();
	public ProfesorBean selectById(int id);
}
