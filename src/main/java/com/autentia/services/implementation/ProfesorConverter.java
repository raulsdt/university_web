package com.autentia.services.implementation;

import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.autentia.bean.ProfesorBean;
import com.autentia.dao.ProfesorRepository;

/**
 * Clase que permite la conversion de la instancia del objeto Profesor a texto y viceversa
 * @author Ra�l
 *
 */


@Component
@SessionScoped
public class ProfesorConverter implements Converter {

	@Autowired
	ProfesorRepository profRep;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		try {
			return profRep.selectProfesorById(Integer.parseInt(value));
		} catch (NumberFormatException e) {
			throw new ConverterException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Conversion Error",
					"Not a valid profesor id."));
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		return String.valueOf(((ProfesorBean) value).getId());
	}

}
