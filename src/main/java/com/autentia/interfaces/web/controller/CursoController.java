package com.autentia.interfaces.web.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.autentia.bean.CursoBean;
import com.autentia.bean.ProfesorBean;
import com.autentia.interfaces.web.form.CursoForm;
import com.autentia.services.implementation.CursoServiceImpl;
import com.autentia.services.implementation.ProfesorServiceImpl;

@Controller
@ManagedBean
@SessionScoped
public class CursoController {

	private Log m_log = LogFactory.getLog(CursoController.class);
	
	@Autowired
	private CursoServiceImpl cursoService;
	
	@Autowired
	private ProfesorServiceImpl profesorService;
	
	@Autowired
	public CursoForm cursoForm;
	
	private List<CursoBean> listCursos;
	private List<ProfesorBean> listProfesores;
	
//	@PostConstruct
//	public void init(){
//		listCursos = cursoService.selectAll();
//		listProfesores = profesorService.selectAll();
//	}
	
	public void createCurso() {
        CursoBean entity = new CursoBean();
        entity.setNivelCurso(cursoForm.getNivelCurso());
        entity.setNombreCurso(cursoForm.getTituloCurso());
        entity.setHorasCurso(cursoForm.getHorasCurso());
        entity.setProfesor(cursoForm.getProfesor());
        if(cursoForm.getNombreFichero() != null && !cursoForm.getNombreFichero().equals("")){
        	entity.setNombreFichero(cursoForm.getNombreFichero());
        	entity.setFileBase64(cursoForm.getFileBase64());
        }

        cursoService.create(entity);
        
        
        cursoForm.cleanForm();
        listCursos = cursoService.selectAll();
    }
	
	public List<CursoBean> getListCursos(){	
		if(listCursos == null)
			listCursos = cursoService.selectAll();
		return listCursos;
			
	}
	
	
	public List<ProfesorBean> getListProfesores(){
		listProfesores =  profesorService.selectAll();
		return listProfesores;
	}
	
	/**
	 * Manejador que recibe el evento de subida de fichero (Codificacion en Base64)
	 * @param event Evento disparado por componente fileUpload (Primefaces/JSF 2)
	 */

	public void handleFileUpload(FileUploadEvent event) {

		if (event.getFile() == null) {
			m_log.error("Failure uploading file.");
			return;
		}

		try {
			
			cursoForm.setNombreFichero(event.getFile().getFileName());
			cursoForm.setFileBase64(Base64.getEncoder().encodeToString(
					IOUtils.toByteArray(event.getFile().getInputstream())));
			FacesMessage message = new FacesMessage("Fichero ", event.getFile()
					.getFileName() + " subido correctamente.");
			FacesContext.getCurrentInstance().addMessage(null, message);
			m_log.error("Event download file.");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	
	/** Getter y Setter **/
	
	public CursoServiceImpl getCursoService() {
		return cursoService;
	}

	public void setCursoService(CursoServiceImpl cursoService) {
		this.cursoService = cursoService;
	}

	public ProfesorServiceImpl getProfesorService() {
		return profesorService;
	}

	public void setProfesorService(ProfesorServiceImpl profesorService) {
		this.profesorService = profesorService;
	}

	public CursoForm getCursoForm() {
		return cursoForm;
	}

	public void setCursoForm(CursoForm cursoForm) {
		this.cursoForm = cursoForm;
	}
	
	
}
