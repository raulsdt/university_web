package com.autentia.interfaces.web.exception;


import javax.inject.Named;

import org.springframework.webflow.engine.FlowExecutionExceptionHandler;
import org.springframework.webflow.engine.RequestControlContext;
import org.springframework.webflow.execution.FlowExecutionException;

/**
 * Clase de gestion de excepciones en FLOW de Spring
 * @author Ra�l S.
 *
 */
@Named
public class DownloadExceptionHandler implements FlowExecutionExceptionHandler{


	@Override
	public boolean canHandle(FlowExecutionException exception) {
	    return true;
	}

	@Override
	public void handle(FlowExecutionException exception,RequestControlContext context) {

	}
}
