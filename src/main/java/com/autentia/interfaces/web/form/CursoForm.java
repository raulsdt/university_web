package com.autentia.interfaces.web.form;

import org.springframework.stereotype.Component;

import com.autentia.bean.ProfesorBean;

@Component
public class CursoForm {
	
	public static final String EL_NAME = "#{cursoForm}";
	
	private  Boolean activo;
	
	private ProfesorBean profesor;
	
	private String tituloCurso;
	
	private int nivelCurso;
	
	private int horasCurso;
	
	private String nombreFichero;
	
	private String fileBase64;
	
	public void cleanForm(){
		activo = null;
		profesor = null;
		tituloCurso = null;
		nivelCurso = 0;
		horasCurso = 0;
		nombreFichero = null;
		fileBase64= null;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public ProfesorBean getProfesor() {
		return profesor;
	}

	public void setProfesor(ProfesorBean profesor) {
		this.profesor = profesor;
	}

	public String getTituloCurso() {
		return tituloCurso;
	}

	public void setTituloCurso(String tituloCurso) {
		this.tituloCurso = tituloCurso;
	}

	public int getNivelCurso() {
		return nivelCurso;
	}

	public void setNivelCurso(int nivelCurso) {
		this.nivelCurso = nivelCurso;
	}

	public int getHorasCurso() {
		return horasCurso;
	}

	public void setHorasCurso(int horasCurso) {
		this.horasCurso = horasCurso;
	}

	public String getNombreFichero() {
		return nombreFichero;
	}

	public void setNombreFichero(String nombreFichero) {
		this.nombreFichero = nombreFichero;
	}

	public String getFileBase64() {
		return fileBase64;
	}

	public void setFileBase64(String fileBase64) {
		this.fileBase64 = fileBase64;
	}
	
	
	
	
	
}
