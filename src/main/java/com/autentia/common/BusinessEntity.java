package com.autentia.common;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class BusinessEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5198982070058630636L;
	
	/**
     * Id.
     */
    protected int id;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

	@Override
	public int hashCode(){
	    return new HashCodeBuilder()
	        .append(getId())
	        .toHashCode();
	}

	@Override
	public boolean equals(final Object obj){
		if (obj == null) { return false; }
		   if (obj == this) { return true; }
		   if (obj.getClass() != getClass()) {
		     return false;
		   }
	        final BusinessEntity other = (BusinessEntity) obj;
	        return new EqualsBuilder()
	            .append(id, other.id)
	            .isEquals();
	    
	}
    
    

}