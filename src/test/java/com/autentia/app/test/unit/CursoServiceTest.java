package com.autentia.app.test.unit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.autentia.bean.CursoBean;
import com.autentia.bean.ProfesorBean;
import com.autentia.dao.CursoRepository;

@RunWith(MockitoJUnitRunner.class)
public class CursoServiceTest {

	@Mock
	CursoRepository cursoService;
	
	CursoBean curso;
	
	@Before
	public void init() throws SQLException, Exception {
		curso = new CursoBean();
		curso.setId(3);
		curso.setNombreCurso("Curso de prueba");
		curso.setHorasCurso(25);
		
		
		ProfesorBean mockedProfesor = mock(ProfesorBean.class);
		when(mockedProfesor.getId()).thenReturn(1);
		curso.setProfesor(mockedProfesor);
	}
	
	
	@Test
	public void testSelectAllCursos()
	{
		
		
		List<CursoBean> lista = new ArrayList<CursoBean>();
		lista.add(curso);
		
		Mockito.when(cursoService.selectAll()).thenReturn(lista);
		
		List<CursoBean> cursos =  cursoService.selectAll();
		Assert.assertNotNull(cursos);
		assertThat(cursos.size(), is(1));
		assertThat(cursos.get(0).getNombreCurso(), is("Curso de prueba"));
		assertThat(cursos.get(0).getHorasCurso(), is(25));
		
	}
	
	@Test
	public void insertNewCurso(){
		try{
			cursoService.insert(curso);
			assert true;
		}catch(Exception e){
			assert false;
		}
		
		
		
	}
	
	
	
	
	
	
}
